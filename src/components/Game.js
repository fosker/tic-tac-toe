import Player from './Player';
import Board from './Board/Board';

class Game {
    constructor(params) {
        this.board = new Board(params);

        this.playerX = new Player('Player-X', 'X');
        this.playerO = new Player('Player-O', 'O');

        this.currentPlayer = this.playerX;

        this.active = true;

        this.winningCombos = [
            [0, 1, 2], [3, 4, 5], [6, 7, 8],
            [0, 3, 6], [1, 4, 7], [2, 5, 8],
            [0, 4, 8], [2, 4, 6]
        ];

        if (params) {
            let playerXpositions = this.board.getPositionsBySymbol(this.playerX.symbol);
            let playerOpositions = this.board.getPositionsBySymbol(this.playerO.symbol);

            if (params.player === 'X' && playerOpositions.length < playerXpositions.length) {
                this.currentPlayer = this.playerO;
            }

            if (params.player === 'O' && playerXpositions.length < playerOpositions.length) {
                this.currentPlayer = this.playerX;
            }

            this.checkWin(this.playerX);
            this.checkWin(this.playerO);
        }

        this.initListeners();
    }

    move(position) {
        if (this.board.isEmpty(position) && this.active) {
            this.board.drawMove(position, this.currentPlayer.symbol);
            this.writeHistory(this.currentPlayer.name + ' filled cell #' + position);

            this.checkWin(this.currentPlayer);
            this.changeTurn();
        }
    }

    checkWin(player) {
        let playerPositions = this.board.getPositionsBySymbol(player.symbol);

        if (playerPositions.length > 4) {
            this.active = false;

            this.writeHistory('Tie!');
        }

        this.winningCombos.forEach((combo) => {
            if (arrayContainsArray(playerPositions, combo)) {
                this.active = false;

                this.writeHistory('Winner ' + this.currentPlayer.name);
            }
        });
    }

    changeTurn() {
        this.currentPlayer = this.currentPlayer === this.playerX ? this.playerO : this.playerX;
    }

    initListeners() {
        document.getElementById("board").addEventListener("click", (event) => {
            if (event.target.classList.contains("position")) {
                this.move(event.target.dataset.position);
            }
        });
        document.getElementById("link-generator").addEventListener("click", (event) => {
            let url = "";

            this.board.cells.forEach((cell, position) => {
                url += 'cell' + position + "=" + cell.symbol + "&";
            });

            url += 'player' + "=" + this.currentPlayer.symbol;

            let link = window.location.host + '?' + url;

            document.getElementById("game-link").href = link;
            document.getElementById("game-link").innerHTML = link;
        });
    }

    writeHistory(text) {
        let li = document.createElement("li");
        li.innerHTML = text;
        document.getElementById("game-history").appendChild(li);
    }
}

function arrayContainsArray (superset, subset) {
    if (subset.length === 0) {
        return false;
    }
    return subset.every(function (value) {
        return (superset.indexOf(value) >= 0);
    });
}

export default Game;
