/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Game = __webpack_require__(1);

var _Game2 = _interopRequireDefault(_Game);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var params = getUrlParams(window.location.search.slice(1));

new _Game2.default(params);

function getUrlParams(url) {
    var hash = void 0;
    var obj = {};
    var hashes = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        obj[hash[0]] = hash[1];
    }

    return obj;
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Player = __webpack_require__(2);

var _Player2 = _interopRequireDefault(_Player);

var _Board = __webpack_require__(3);

var _Board2 = _interopRequireDefault(_Board);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Game = function () {
    function Game(params) {
        _classCallCheck(this, Game);

        this.board = new _Board2.default(params);

        this.playerX = new _Player2.default('Player-X', 'X');
        this.playerO = new _Player2.default('Player-O', 'O');

        this.currentPlayer = this.playerX;

        this.active = true;

        this.winningCombos = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];

        if (params) {
            var playerXpositions = this.board.getPositionsBySymbol(this.playerX.symbol);
            var playerOpositions = this.board.getPositionsBySymbol(this.playerO.symbol);

            if (params.player === 'X' && playerOpositions.length < playerXpositions.length) {
                this.currentPlayer = this.playerO;
            }

            if (params.player === 'O' && playerXpositions.length < playerOpositions.length) {
                this.currentPlayer = this.playerX;
            }

            this.checkWin(this.playerX);
            this.checkWin(this.playerO);
        }

        this.initListeners();
    }

    _createClass(Game, [{
        key: 'move',
        value: function move(position) {
            if (this.board.isEmpty(position) && this.active) {
                this.board.drawMove(position, this.currentPlayer.symbol);
                this.writeHistory(this.currentPlayer.name + ' filled cell #' + position);

                this.checkWin(this.currentPlayer);
                this.changeTurn();
            }
        }
    }, {
        key: 'checkWin',
        value: function checkWin(player) {
            var _this = this;

            var playerPositions = this.board.getPositionsBySymbol(player.symbol);

            if (playerPositions.length > 4) {
                this.active = false;

                this.writeHistory('Tie!');
            }

            this.winningCombos.forEach(function (combo) {
                if (arrayContainsArray(playerPositions, combo)) {
                    _this.active = false;

                    _this.writeHistory('Winner ' + _this.currentPlayer.name);
                }
            });
        }
    }, {
        key: 'changeTurn',
        value: function changeTurn() {
            this.currentPlayer = this.currentPlayer === this.playerX ? this.playerO : this.playerX;
        }
    }, {
        key: 'initListeners',
        value: function initListeners() {
            var _this2 = this;

            document.getElementById("board").addEventListener("click", function (event) {
                if (event.target.classList.contains("position")) {
                    _this2.move(event.target.dataset.position);
                }
            });
            document.getElementById("link-generator").addEventListener("click", function (event) {
                var url = "";

                _this2.board.cells.forEach(function (cell, position) {
                    url += 'cell' + position + "=" + cell.symbol + "&";
                });

                url += 'player' + "=" + _this2.currentPlayer.symbol;

                var link = window.location.host + '?' + url;

                document.getElementById("game-link").href = link;
                document.getElementById("game-link").innerHTML = link;
            });
        }
    }, {
        key: 'writeHistory',
        value: function writeHistory(text) {
            var li = document.createElement("li");
            li.innerHTML = text;
            document.getElementById("game-history").appendChild(li);
        }
    }]);

    return Game;
}();

function arrayContainsArray(superset, subset) {
    if (subset.length === 0) {
        return false;
    }
    return subset.every(function (value) {
        return superset.indexOf(value) >= 0;
    });
}

exports.default = Game;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Player = function Player(name, symbol) {
    _classCallCheck(this, Player);

    this.name = name;
    this.symbol = symbol;
};

exports.default = Player;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Cell = __webpack_require__(4);

var _Cell2 = _interopRequireDefault(_Cell);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Board = function () {
    function Board(params) {
        _classCallCheck(this, Board);

        if (params) {
            this.cells = [new _Cell2.default(params.cell0), new _Cell2.default(params.cell1), new _Cell2.default(params.cell2), new _Cell2.default(params.cell3), new _Cell2.default(params.cell4), new _Cell2.default(params.cell5), new _Cell2.default(params.cell6), new _Cell2.default(params.cell7), new _Cell2.default(params.cell8)];

            this.draw();
        } else {
            this.cells = [new _Cell2.default(), new _Cell2.default(), new _Cell2.default(), new _Cell2.default(), new _Cell2.default(), new _Cell2.default(), new _Cell2.default(), new _Cell2.default(), new _Cell2.default()];
        }
    }

    _createClass(Board, [{
        key: "draw",
        value: function draw() {
            this.cells.forEach(function (cell, position) {
                document.querySelectorAll("[data-position='" + position + "']")[0].innerHTML = cell.symbol;
            });
        }
    }, {
        key: "drawMove",
        value: function drawMove(position, symbol) {
            this.cells[position].symbol = symbol;

            document.querySelectorAll("[data-position='" + position + "']")[0].innerHTML = symbol;
        }
    }, {
        key: "getPositionsBySymbol",
        value: function getPositionsBySymbol(symbol) {
            var positions = [];

            this.cells.forEach(function (cell, position) {
                if (cell.symbol === symbol) {
                    positions.push(position);
                }
            });

            return positions;
        }
    }, {
        key: "isEmpty",
        value: function isEmpty(position) {
            return this.cells[position].symbol === '';
        }
    }]);

    return Board;
}();

exports.default = Board;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Cell = function Cell(symbol) {
    _classCallCheck(this, Cell);

    this.symbol = symbol ? symbol : '';
};

exports.default = Cell;

/***/ })
/******/ ]);