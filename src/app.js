import Game from './components/Game'

const params = getUrlParams(window.location.search.slice(1));

new Game(params);

function getUrlParams(url) {
    let hash;
    let obj = {};
    let hashes = url.slice(url.indexOf('?') + 1).split('&');
    for (let i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        obj[hash[0]] = hash[1];
    }

    return obj;
}