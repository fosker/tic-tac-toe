import Cell from './Cell'

class Board {
    constructor(params) {
        if (params) {
            this.cells = [
                new Cell(params.cell0), new Cell(params.cell1), new Cell(params.cell2),
                new Cell(params.cell3), new Cell(params.cell4), new Cell(params.cell5),
                new Cell(params.cell6), new Cell(params.cell7), new Cell(params.cell8)
            ];

            this.draw();
        } else {
            this.cells = [
                new Cell(), new Cell(), new Cell(),
                new Cell(), new Cell(), new Cell(),
                new Cell(), new Cell(), new Cell()
            ];
        }
    }

    draw() {
        this.cells.forEach(function(cell, position) {
            document.querySelectorAll("[data-position='" + position + "']")[0].innerHTML = cell.symbol;
        });
    }

    drawMove(position, symbol) {
        this.cells[position].symbol = symbol;

        document.querySelectorAll("[data-position='" + position + "']")[0].innerHTML = symbol;
    }

    getPositionsBySymbol(symbol) {
        let positions = [];

        this.cells.forEach(function (cell, position) {
            if (cell.symbol === symbol) {
                positions.push(position);
            }
        });

        return positions;
    }

    isEmpty(position) {
        return this.cells[position].symbol === '';
    }
}

export default Board;